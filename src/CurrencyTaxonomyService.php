<?php

namespace Drupal\currency_taxonomy;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a service to manage currencies.
 *
 * @package Drupal\currency_taxonomy
 */
class CurrencyTaxonomyService implements CurrencyTaxonomyServiceInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CurrencyTaxonomyService instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager instance.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function createCurrency(array $data) {
    $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    $term = $taxonomy_storage->create([
      'vid' => 'currency',
      'name' => !empty($data['currency']) ? $data['currency'] : (!empty($data['field_currency_iso']) ? $data['field_currency_iso'] : ''),
    ]);

    foreach ($data as $field => $value) {
      $term->set($field, $value);
    }

    $term->save();

    return $term;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrencyByCode($code) {
    return $this->getCurrency(['field_currency_iso' => ['value' => $code]]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrencyByNumber($number) {
    return $this->getCurrency(['field_currency_number' => ['value' => $number]]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrencyByCountry($country) {
    return $this->getCurrency(['field_currency_country' => ['value' => $country, 'op' => 'CONTAINS']]);
  }

  /**
   * Helper to get currency by conditions.
   *
   * @param array $conditions
   *   An array of conditions to filter currencies.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   Currency entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCurrency(array $conditions) {
    $taxonomy_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    $query = $taxonomy_storage->getQuery();
    $query->accessCheck();
    $query->condition('vid', 'currency');

    foreach ($conditions as $field => $condition) {
      $op = !empty($condition['op']) ? $condition['op'] : NULL;
      $query->condition($field, $condition['value'], $op);
    }

    if ($results = $query->execute()) {
      $tid = reset($results);

      return $taxonomy_storage->load($tid);
    }

    return NULL;
  }

}
