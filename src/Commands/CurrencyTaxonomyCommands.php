<?php

namespace Drupal\currency_taxonomy\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;

/**
 * Class CurrencyTaxonomyCommands. The base class for Drush commands.
 */
class CurrencyTaxonomyCommands extends DrushCommands {

  /**
   * Returns the entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a CurrencyTaxonomyCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Re-import all currency codes from the file.
   *
   * @throws \Exception
   *
   * @usage drush currency-taxonomy:import
   *   Re-import all currency codes from the file.
   *
   * @command currency-taxonomy:import
   * @aliases cti
   */
  public function deletePrivateFields() {
    if (!$this->io()->confirm(dt('Are you sure you want to re-import all currency codes?'))) {
      throw new UserAbortException();
    }

    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $query = $term_storage->getQuery();
    $query->condition('vid', 'currency');
    $query->accessCheck(FALSE);

    if ($tids = $query->execute()) {
      $terms = $term_storage->loadMultiple($tids);
      $term_storage->delete($terms);
    }

    currency_taxonomy_add_terms();
    $this->output()->writeln(dt('All currency codes have been re-imported.'));
  }

}
