<?php

namespace Drupal\currency_taxonomy;

/**
 * Provides an interface for currency service.
 *
 * @package Drupal\currency_taxonomy
 */
interface CurrencyTaxonomyServiceInterface {

  /**
   * Helper to create a new currency.
   *
   * @param array $data
   *   Currency data (ISO code, number, etc).
   *
   * @return \Drupal\taxonomy\TermInterface
   *   Currency entity.
   */
  public function createCurrency(array $data);

  /**
   * Helper to get currency by its code.
   *
   * @param string $code
   *   Currency ISO code.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   Currency entity.
   */
  public function getCurrencyByCode($code);

  /**
   * Helper to get currency by its number.
   *
   * @param string $number
   *   Currency number.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   Currency entity.
   */
  public function getCurrencyByNumber($number);

  /**
   * Helper to get currency by its country.
   *
   * @param string $country
   *   Currency country.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   Currency entity.
   */
  public function getCurrencyByCountry($country);

}
