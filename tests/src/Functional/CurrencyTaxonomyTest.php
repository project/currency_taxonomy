<?php

namespace Drupal\Tests\currency_taxonomy\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Tests\BrowserTestBase;

/**
 * Class CurrencyTaxonomyTest. The base class for testing the currency service.
 */
class CurrencyTaxonomyTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['taxonomy', 'currency_taxonomy'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test currency service.
   */
  public function testCurrencyService() {
    /** @var \Drupal\currency_taxonomy\CurrencyTaxonomyServiceInterface $service */
    $service = \Drupal::service('currency_taxonomy.service');

    /** @var \Drupal\Core\Extension\ModuleExtensionList $extension_list */
    $extension_list = \Drupal::service('extension.list.module');
    $module_path = $extension_list->getPath('currency_taxonomy');

    if ($content = file_get_contents("{$module_path}/currency_codes.json")) {
      $data = Json::decode($content);
      $rand_keys = array_rand($data, 10);

      // Test random currency codes.
      foreach ($rand_keys as $key) {
        $item = $data[$key];

        if (isset($item['iso_code'], $item['country']) && $term = $service->getCurrencyByCode($item['iso_code'])) {
          $this->assertEquals($item['country'], $term->get('field_currency_country')->getString());
        }
      }
    }
  }

}
