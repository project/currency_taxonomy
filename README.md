# Currency taxonomy

This module creates currency taxonomy with ISO code and Country Name enlisted in
it. The module provides list of all available currencies. Each list term name
has value in the "Currency Name(ISO code)" format where ISO code is Alphabetic
ISO code of that currency.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/currency_taxonomy).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/currency_taxonomy).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

This module has no configuration.


## Troubleshooting

- Currency taxonomy module uses Term::save() to add terms
  based on currency. Term::save()) will be run for each
  term - and this will give performance issues
  on enabling module


## Maintainers

- Richard Papp - [boromino](https://www.drupal.org/u/boromino)
- Neha Pandya - [nehapandya55](https://www.drupal.org/u/nehapandya55)
- AstonVictor - [astonvictor](https://www.drupal.org/u/astonvictor)
